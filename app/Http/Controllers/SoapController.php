<?php

namespace App\Http\Controllers;

use SoapClient;
use SoapFault;

class SoapController
{

    public function search($searchString) {

        try {
            $client = new SoapClient("http://test.soa.energo.lv:8011/DMBServices/Private/DMBService?wsdl");

            $params = [
                "MMIdentification" =>
                    [
                        "username" => "*DAMBO*",
                        "password" => "u4U3hHiKa5ky4SDw",
                        "supplierEIC" => "mcnffDxWh5NEZ6BwkAvUNtsVdwaUSUeb",
                        "messageId" => "string"
                    ]
            ];

            $response = $client->__soapCall("GetSuppliersList", [$params]);

        } catch ( SoapFault $e ) {
            return ['error' => 'Sorry... our service is down'];
        }

        $result = [];

        if (is_numeric($searchString)) {
           foreach ($response->suppliersList->SuppliersEntry as $entry) {
                if (strpos($entry->registrationNumber, $searchString) !== false && $entry->isActive === 'Y' ) {
                    $result[] = $entry;
                }
           }
        } else {
            foreach ($response->suppliersList->SuppliersEntry as $entry) {
                if (strpos(strtolower($entry->supplierName), strtolower($searchString)) !== false && $entry->isActive === 'Y' ) {
                    $result[] = $entry;
                }
            }
        }

        return ['result' => $result];

    }

}